#pragma once
#include "Hand.h"
#include <iostream>
enum Action
{
	HIT,
	STAND
};

class Player
{
public:
	Player(const char* name);
	~Player();

	const char* GetName() const;
	const Hand& GetHand() const;
	Hand& GetHand();
	Action GetAction() const;
	friend std::ostream& operator<<(std::ostream& os, Player player);
private:
	char* m_name;
	Hand m_hand;
};

bool operator < (const Player& lhs, const Player& rhs);
bool operator > (const Player& lhs, const Player& rhs);
bool operator == (const Player& lhs, const Player& rhs);
bool operator != (const Player& lhs, const Player& rhs);
bool operator <= (const Player& lhs, const Player& rhs);
bool operator >= (const Player& lhs, const Player& rhs);
