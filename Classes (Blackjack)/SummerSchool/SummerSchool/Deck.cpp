
#include "Deck.h"
#include "Card.h"

#include <algorithm>

Deck::Deck() : m_cards(nullptr), m_numCards(52)
{
	Suit suits[] =
	{
		HEARTS,
		DIAMONDS,
		CLUBS,
		SPADES
	};
	size_t numSuits = sizeof(suits) / sizeof(Suit);

	Rank ranks[] =
	{
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'T',
		'J',
		'Q',
		'K',
		'A'
	};
	size_t numRanks = sizeof(ranks) / sizeof(Rank);

	m_cards = new Card*[numRanks * numSuits];
	
	size_t counter = 0;
	for (Suit suit : suits)
	{
		for (Rank rank : ranks)
		{
			m_cards[counter++] = new Card(suit, rank);
		}
	}
}

Deck::~Deck()
{
	for (size_t i = 0; i < m_numCards; i++)
	{
		delete m_cards[i];
	}
	delete [] m_cards;
}

size_t Deck::GetNumberOfCards() const
{
	return m_numCards;
}

Card ** Deck::GetCards() const
{
	return m_cards;
}

Deck & Deck::operator!()
{
	if (m_numCards)
	{
		std::random_shuffle(m_cards, m_cards + m_numCards);
	}
	return *this;
}
