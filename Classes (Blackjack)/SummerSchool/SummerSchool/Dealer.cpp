
#include "Dealer.h"
#include "Card.h"
#include "Player.h"

#include <algorithm>
#include <ctime>
#include <cstdlib>

Dealer::Dealer() : m_deck(), m_shoe()
{
}

Dealer::~Dealer()
{
}

void Dealer::Shuffle()
{
	!m_deck;
	m_shoe.SetCards(m_deck.GetNumberOfCards(), m_deck.GetCards());
	//size_t numCards = m_deck.GetNumberOfCards();
	//Card** cards = m_deck.GetCards();
	//
	//std::random_shuffle(cards, cards + numCards);
	//
	//m_shoe.SetCards(numCards, cards);
}

void Dealer::Deal(Player* player)
{
	Card* card = m_shoe.NextCard();
	if (card == nullptr) return;

	player->GetHand() = player->GetHand() + card;
}

void Dealer::operator<<(Player* player)
{
	Card* card = m_shoe.NextCard();

	if (card == nullptr)
		return;

	if (m_shoe)
		player->GetHand() = player->GetHand() << m_shoe;

	return;


	//player->GetHand().AddCard(card);
	//player->GetHand() = player->GetHand() + card;
}
