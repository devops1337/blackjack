
#include "Card.h"


Card::Card(Suit suit, Rank rank) : m_suit(suit), m_rank(rank)
{
}

void Card::Print()
{
	printf("%c of ", m_rank);
	switch (m_suit)
	{
	case HEARTS:  printf("HEARTS"); break;
	case DIAMONDS:	 printf("DIAMONDS"); break;
	case CLUBS:  printf("CLUBS"); break;
	case SPADES:  printf("SPADES"); break;
	}
	printf("\n");
}

Card::~Card()
{
}

std::ostream & operator<<(std::ostream & os, const Card & card)
{
	os << card.m_rank << " of ";
	switch (card.m_suit)
	{
	case HEARTS:  os<<"HEARTS"; break;
	case DIAMONDS:	 os<<"DIAMONDS"; break;
	case CLUBS:  os<<"CLUBS"; break;
	case SPADES:  os<<"SPADES"; break;
	}
	return os << '\n';
}
