#pragma once

#include "Dealer.h"

class Player;

class Game
{
public:
	Game(size_t numPlayers, Player** players);
	~Game();

	void Play();

private:
	void DealInitialCards();
	void GameLoop();
	void PrintPlayers() const;

	size_t m_numPlayers;
	Player** m_players;
	Dealer m_dealer;
};

