#pragma once

class Card;
class Shoe
{
public:
	Shoe();
	~Shoe();

	void SetCards(size_t numCards, Card** cards);
	Card* NextCard();

	operator bool();

private:
	size_t m_numCards;
	Card** m_cards;
	size_t m_nextCardIndex;
};

