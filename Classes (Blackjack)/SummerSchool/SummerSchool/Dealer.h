#pragma once
#include "Deck.h"
#include "Shoe.h"

class Player;
class Dealer
{
public:
	Dealer();
	~Dealer();

	void Shuffle();
	void Deal(Player* player);
	void operator<<(Player* player);

private:
	Deck m_deck;
	Shoe m_shoe;
};

