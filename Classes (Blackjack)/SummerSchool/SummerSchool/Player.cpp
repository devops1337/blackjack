
#include "Player.h"
#include <cstring>

Player::Player(const char * name) : m_name(nullptr), m_hand()
{
	m_name = new char[strlen(name) + 1]; // add 1 for '\0'
	strcpy(m_name, name);
}

Player::~Player()
{
	delete[] m_name;
	m_name = nullptr;
}

const char * Player::GetName() const
{
	return m_name;
}

const Hand & Player::GetHand() const
{
	return m_hand;
}

Hand& Player::GetHand()
{
	return m_hand;
}

Action Player::GetAction() const
{
	Action returnValue = STAND;
	if (m_hand.GetValue() < 17) returnValue = HIT;

	return returnValue;
}

std::ostream & operator<<(std::ostream & os, Player player)
{	
	return os << player.m_name << player.m_hand;
}

bool operator<(const Player& lhs, const Player& rhs)
{
		return lhs.GetHand().GetValue() < rhs.GetHand().GetValue();
}

bool operator>(const Player& lhs, const Player& rhs)
{
		return !(lhs < rhs);
}


bool operator == (const Player& lhs, const Player& rhs)
{
	return lhs.GetHand().GetValue() == rhs.GetHand().GetValue();
}

bool operator != (const Player& lhs, const Player& rhs)
{
	return !(lhs == rhs);
}

bool operator <= (const Player& lhs, const Player& rhs)
{
	return lhs < rhs || lhs == rhs;
}

bool operator >= (const Player& lhs, const Player& rhs)
{
	return !(lhs <= rhs);
}