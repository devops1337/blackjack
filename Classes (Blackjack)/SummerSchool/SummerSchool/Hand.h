#pragma once
#include <iostream>
#include "Shoe.h"
class Card;

class Hand
{
public:
	Hand();
	Hand(Card** m_cards, size_t m_numCards);
	~Hand();

	void AddCard(Card* card);
	size_t GetValue() const;
	friend std::ostream& operator<< (std::ostream& os, const Hand& hand);
	Hand& operator << (Shoe& shoe);
	const Hand operator + (Card* card);
	void Print();

private:
	size_t m_numCards;
	Card** m_cards;
	
	
};

bool operator < (const Hand& hand, int value);
bool operator > (const Hand& hand, int value);
bool operator == (const Hand& hand, int value);

