#pragma once

#include <ostream>

enum Suit {
	HEARTS, 
	DIAMONDS,
	CLUBS, 
	SPADES
};

typedef char Rank;

class Card
{
public:
	Card(Suit suit, Rank rank);
	Card(const Card& other) = delete;
	Card& operator= (const Card& other) = delete;

	Rank GetRank() const { return m_rank; }

	friend std::ostream& operator << (std::ostream& os, const Card& card);
	void Print();

	~Card();

private:


	Suit m_suit;
	Rank m_rank;
};

