#include "Hand.h"
#include "Card.h"

#include <iostream>

Hand::Hand() : Hand(nullptr, 0)
{
	// empty
}

Hand::Hand(Card** m_cards, size_t m_numCards) : m_cards(m_cards), m_numCards(m_numCards)
{
	
}

Hand::~Hand()
{
	// Empty
}

void Hand::AddCard(Card* card)
{
	size_t newNumCards = m_numCards + 1;
	Card** newCards = new Card*[newNumCards];

	for (size_t idx = 0; idx < m_numCards; ++idx)
	{
		newCards[idx] = m_cards[idx];
	}

	delete[] m_cards;
	m_cards = nullptr;

	// Set the new card at last position
	newCards[newNumCards - 1] = card;

	m_cards = newCards;
	m_numCards = newNumCards;
}

size_t Hand::GetValue() const
{
	size_t val = 0;
	size_t numAces = 0;
	for (int idx = 0; idx < m_numCards; ++idx)
	{
		Rank rank = m_cards[idx]->GetRank();
		size_t value = 0;
		if (rank >= '2' && rank <= '9')
		{
			value = rank - '0';
		}
		else if (rank == 'A')
		{
			value = 11;
			++numAces;
		}
		else
		{
			value = 10;
		}

		val += value;
	}

	while (val > 21 && numAces > 0)
	{
		val -= 10;
		--numAces;
	}
	return val;
}


Hand& Hand::operator<<(Shoe& shoe)
{
	AddCard(shoe.NextCard());
	return *this;
}

const Hand Hand::operator+(Card* card)
{
	size_t newNumCards = m_numCards + 1;
	Card** newCards = new Card * [newNumCards];

	for (size_t idx = 0; idx < m_numCards; ++idx)
	{
		newCards[idx] = m_cards[idx];
	}

	//delete[] m_cards;
	//m_cards = nullptr;

	// Set the new card at last position
	newCards[newNumCards - 1] = card;

	//m_cards = newCards;
	//m_numCards = newNumCards;
	return Hand(newCards,newNumCards);
}

void Hand::Print()
{
	for (int idx = 0; idx < m_numCards; ++idx)
	{
		printf("\t");
		std::cout << *m_cards[idx];
	}
}

std::ostream& operator<<(std::ostream& os, const Hand& hand)
{
	for (int idx = 0; idx < hand.m_numCards; ++idx)
	{
		printf("\t");
		os << *hand.m_cards[idx];
	}
	return os;
}

bool operator<(const Hand & hand, int value)
{
	return hand.GetValue() < value;
}

bool operator>(const Hand & hand, int value)
{
	return !(hand < value);
}

bool operator==(const Hand & hand, int value)
{
	return hand.GetValue() == value;
}
