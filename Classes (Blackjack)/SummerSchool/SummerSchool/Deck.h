#pragma once

class Card;

class Deck
{
public:
	Deck();
	~Deck();

	size_t GetNumberOfCards() const;
	Card** GetCards() const;

	Deck& operator!();
private:
	Card** m_cards;
	size_t m_numCards;
};

