#pragma once
#include "Card.h"
class Deck
{
public:
	Deck();
	Deck(int m_numCards, Card* m_cards);
	~Deck();
public:
	Card* GetCards();
	int GetNumberOfCards();
	void SetNumberOfCards(int numCards);
private:
	Card* m_cards;
	int m_numCards;
};

