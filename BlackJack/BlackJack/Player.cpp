#include "Player.h"

#include <iostream>
#include <regex>

Player::Player(std::string name, Hand hand)
	: m_name(name),
	m_hand(hand)
{
}

const std::string & Player::GetName() const
{
	return m_name;
}

const Hand & Player::GetHand() const
{
	return m_hand;
}

Player::Action Player::GetAction() const
{
	std::cout << "Press 0 for Hit, 1 for Stand.\n";
	std::string choice{};
	std::getline(std::cin, choice);

	try
	{
		int choiceInteger = std::stoi(choice);
		if (choiceInteger == 0)
			return Player::Action::Hit;
		else
			return Player::Action::Stand;
	}
	catch (const std::invalid_argument& e)
	{
		std::cerr << "The input is invalid.";
	}
}
