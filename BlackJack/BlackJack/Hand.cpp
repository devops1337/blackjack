#include "Hand.h"



Hand::Hand() 
	: Hand(0, nullptr)
{
}

Hand::Hand(int m_numCards, Card * m_cards) 
	: m_numCards(m_numCards),
	 m_cards(m_cards)
{	
	m_currentCapacity = m_numCards;
}

Hand::Hand(const Hand & other)
{
	*this = other;
}

Hand & Hand::operator=(const Hand & other)
{
	if (this != &other && other.m_cards)
	{
		if (m_cards)
			delete[] m_cards;

		m_cards = new Card[other.m_numCards];
		for (int index = 0; index < other.m_numCards; ++index)
		{
			m_cards[index] = other.m_cards[index];
		}
		m_numCards = other.m_numCards;
	}
	return *this;
}


Hand::Hand(Hand && other)
{
	*this = other;
}

Hand & Hand::operator=(Hand && other)
{
	if (this != &other && other.m_cards)
	{
		if (m_cards)
			delete[] m_cards;

		m_cards = other.m_cards;
		other.m_cards = nullptr;
		m_numCards = other.m_numCards;
		other.m_numCards = 0;
	}

	return *this;
}

Hand::~Hand()
{
	delete[] m_cards;
}

void Hand::AddCard(Card card)
{
	if (m_currentCapacity == m_numCards)
	{
		ReallocateCards(card, m_numCards + kCapacityOffset);
		return;
	}

	m_cards[m_numCards] = card;
	++m_numCards;
}

unsigned int Hand::GetValue()
{
	uint8_t sum = 0;
	for (int i = 0; i < m_numCards; i++)
	{
		sum += static_cast<uint8_t>(m_cards[i].GetRank());
	}
	return sum;
}

void Hand::Print() const
{
	std::cout << "Number of Cards: " << m_numCards << std::endl;
	std::cout << "Player's Cards: ";
	for (int i = 0; i < m_numCards; i++)
		m_cards[i].Print();
}

void Hand::ReallocateCards(Card card, size_t newCapacity)
{
	Card* newCards = new Card[m_numCards + kCapacityOffset];
	for (int index = 0; index < m_numCards; ++index)
	{
		newCards[index] = m_cards[index];
	}
	newCards[m_numCards] = card;
	++m_numCards;

	delete[] m_cards;
	m_cards = newCards;
	newCards = nullptr;
}
