#pragma once

class Shoe;
#include "Player.h"
#include "Deck.h"
#include "Card.h"
#include <random>
class Dealer
{
public:
	Dealer();
	Dealer(Deck* deck);
	Dealer(Deck* deck, Shoe* shoe);
	~Dealer();
public:
	void Shuffle();
	Deck* Deal();
private:
	Deck* m_deck;
	Shoe* m_shoe;
};
