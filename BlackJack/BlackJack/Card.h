#pragma once
#include <cstdint>
#include <unordered_map>
#include <string>

class Card
{
public:
	enum class Rank : uint8_t
	{
		None = -1,
		Two = 2,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine,
		Ten,
		Ace,
		Jack = 10,
		Queen = 10,
		King = 10
	};
	enum class Suit : uint8_t
	{
		None = -1,
		Hearts,
		Diamonds,
		Clovers,
		Spades
	};

public:
	static const std::unordered_map <Rank, std::string> kRankToString;
	static const std::unordered_map <Suit, std::string> kSuitToString; 
public:

	Card(Rank rank, Suit suit);
	Card();
	Card& operator = (const Card& aCard);

	~Card();

private:

	Rank m_rank;
	Suit m_suit;

public:

	Card::Rank GetRank();
	void Print();

};


	