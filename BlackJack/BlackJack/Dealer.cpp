#include "Dealer.h"

Dealer::Dealer() : m_deck(nullptr), m_shoe(nullptr)
{
}

Dealer::Dealer(Deck * deck) 
	: m_deck(deck)
{
}

Dealer::Dealer(Deck * m_deck, Shoe * m_shoe)
{
}

Dealer::~Dealer()
{
	delete[] m_deck;
	delete[] m_shoe;
}

void Dealer::Shuffle()
{
	std::shuffle(m_deck, m_deck + m_deck->GetNumberOfCards(), std::default_random_engine{});
}

Deck* Dealer::Deal()
{
	m_deck->SetNumberOfCards(m_deck->GetNumberOfCards() - 1);
	return nullptr;
	//return (m_deck->GetCards() + m_deck->GetNumberOfCards());
}

