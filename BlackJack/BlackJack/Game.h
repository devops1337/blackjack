#pragma once

#include <cstddef>

#include "Dealer.h"
#include "Player.h"

class Game
{
public:
	Game(Dealer dealer, std::size_t numberOfPlayers, Player* m_players);
	Game(const Game& other);
	Game& operator = (const Game& other);

	Game(Game&& other);
	Game& operator = (Game&& other);
	~Game();

	void Play();

private:
	void DealInitialCards();
	void GameLoop();
	void PrintPlayers();

	Dealer m_dealer;
	std::size_t m_numPlayers;
	Player* m_players;
};