#include <iostream>
#include "Card.h"
#include "Deck.h"
#include "Dealer.h"

int main()
{
	Card first(Card::Rank::Ace, Card::Suit::Clovers);
	Card second(Card::Rank::Ace, Card::Suit::Diamonds);
	Card third(Card::Rank::Ace, Card::Suit::Spades);
	Card fourth(Card::Rank::King, Card::Suit::Spades);

	Card* cards = new Card[4];
	cards[0] = first;
	cards[1] = second;
	cards[2] = third;
	cards[3] = fourth;

	Deck* deck = new Deck(4, cards);

	Dealer dealer(deck);
	dealer.Shuffle();
	delete deck;
	delete[] cards;
	std::cin.get();
	return 0;
}