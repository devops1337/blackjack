#pragma once
#include <iostream>

#include "Card.h"

class Hand
{
public:
	Hand();
	Hand(int m_numCards, Card* m_cards);
	Hand(const Hand& other);
	Hand& operator = (const Hand& other);

	Hand(Hand&& other);
	Hand& operator = (Hand&& other);
	~Hand();
public:
	void AddCard(Card card);
	unsigned int GetValue();
	void Print() const;
private:
	void ReallocateCards(Card card, size_t newCapacity);
	
	static constexpr int kCapacityOffset = 20;

	int m_currentCapacity;
	int m_numCards;
	Card* m_cards;
};

