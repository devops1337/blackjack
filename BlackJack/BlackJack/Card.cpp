#include "Card.h"

#include <iostream>

const std::unordered_map<Card::Rank, std::string> Card::kRankToString = 
{ 
	{Card::Rank::Two, "Two"},
	{Card::Rank::Three, "Three"},
	{Card::Rank::Four, "Four"},
	{Card::Rank::Five, "Five"},
	{Card::Rank::Six, "Six"},
	{Card::Rank::Seven, "Seven"},
	{Card::Rank::Eight, "Eight"},
	{Card::Rank::Nine, "Nine"},
	{Card::Rank::Ten, "Ten"},
	{Card::Rank::Ace, "Ace"},
	{Card::Rank::Queen, "Queen"},
	{Card::Rank::King, "King"},
	{Card::Rank::Jack, "Jack"},
	{Card::Rank::None, "None"},
};

const std::unordered_map<Card::Suit, std::string> Card::kSuitToString =
{
	{Card::Suit::Diamonds, "Diamonds"},
	{Card::Suit::Hearts, "Hearts"},
	{Card::Suit::Clovers, "Clovers"},
	{Card::Suit::Spades, "Spades"},
	{Card::Suit::None, "None"},
};

Card::Card(Rank rank, Suit suit)
	: m_rank(rank), m_suit(suit)
{}


Card::Card()
{
	m_rank = Rank::None;
	m_suit = Suit::None;
}

Card& Card::operator=(const Card& aCard)
{
	if (this == &aCard)
		return *this;

	m_rank = aCard.m_rank;
	m_suit = aCard.m_suit;
}

Card::~Card()
{
}

Card::Rank Card::GetRank()
{
	return m_rank;
}

void Card::Print()
{
	std::cout << "Card: " << kRankToString.at(m_rank) << " of " << kSuitToString.at(m_suit) << '\n';
}
