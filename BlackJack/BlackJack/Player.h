#pragma once

#include "Hand.h"

#include <string>

class Player
{
public:
	enum class Action : uint8_t
	{
		Hit,
		Stand
	};
public:
	Player(std::string name, Hand hand);

	const std::string& GetName() const;
	const Hand& GetHand() const;
	Action GetAction() const;

private:
	Hand m_hand;
	std::string m_name;
};