#pragma once
class Card;
class Shoe
{
public:
	Shoe();
	Shoe(Card* m_cards, int m_numCards, int m_nextCardIndex);
	~Shoe();
public:
	Card* NextCard();
	void SetCards();
private:
	Card* m_cards;
	int m_nextCardIndex;
	int m_numCards;
};

