#include "Deck.h"

Deck::Deck() : m_numCards(0), m_cards(nullptr)
{

}

Deck::Deck(int m_numberOfCards, Card * m_cards) : m_numCards(m_numberOfCards), m_cards(m_cards)
{

}

Deck::~Deck()
{
	delete[] m_cards;
}

Card* Deck::GetCards()
{
	return m_cards;
}

int Deck::GetNumberOfCards()
{
	return m_numCards;
}

void Deck::SetNumberOfCards(int numCards)
{
	m_numCards = numCards;
}
